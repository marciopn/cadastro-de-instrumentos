package br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.PaginadoAtivoResponseData;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarAtivosGateway;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.apicadastro.infra.configuration.mapper.ObjectMapperUtil;
import br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.entity.JpaAtivoEntity;
import br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.repository.JpaAtivoRepository;
import br.com.itau.instrumentos.base.gateway.BuscarPorIdGateway;
import br.com.itau.instrumentos.base.gateway.DeletarGateway;
import br.com.itau.instrumentos.base.gateway.SalvarGateway;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Repository
public class AtivoDataProvider implements BuscarPorCodigoAtivoGateway, SalvarGateway<Ativo>,
        BuscarPorIdGateway<Ativo, String>, DeletarGateway<String>, BuscarAtivosGateway {

    private final JpaAtivoRepository repository;

    public AtivoDataProvider(JpaAtivoRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<Ativo> buscarPorId(String id) {
        JpaAtivoEntity jpaAtivoEntity = repository.findById(id).orElse(null);

        if (Objects.nonNull(jpaAtivoEntity)) {
            return Optional.of(ObjectMapperUtil.convertTo(jpaAtivoEntity, Ativo.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Ativo salvar(Ativo ativo) {
        JpaAtivoEntity entity = repository.save(ObjectMapperUtil.convertTo(ativo, JpaAtivoEntity.class));

        return ObjectMapperUtil.convertTo(entity, Ativo.class);
    }

    @Override
    public Optional<Ativo> buscarPorCodigoAtivo(String codigo) {
        List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);

        if (jpaAtivos.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(ObjectMapperUtil.convertTo(jpaAtivos.get(0), Ativo.class));
        }
    }

    @Override
    public Boolean deletar(String codigo) {
        List<JpaAtivoEntity> jpaAtivos = repository.findByCodigo(codigo);
        if (!jpaAtivos.isEmpty()) {
            repository.delete(jpaAtivos.get(0));

            return true;
        }

        return false;
    }

    @Override
    public PaginadoAtivoResponse buscarAtivos(Integer pagina, Integer qtdPorPagina) {
        Page<JpaAtivoEntity> pageJpaAtivo = repository.findAll(PageRequest.of(pagina, qtdPorPagina));

        PaginadoAtivoResponse response = new PaginadoAtivoResponse();

        response.setPaginaAtual(pagina);

        response.setQtdRegistrosDaPagina(pageJpaAtivo.getNumberOfElements());

        response.setQtdRegistrosTotais(Math.toIntExact(pageJpaAtivo.getTotalElements()));

        pageJpaAtivo.getContent().forEach(c -> response
                .adicionarAtivo(new PaginadoAtivoResponseData(c.getId(), c.getCodigo(), c.getDescricao(), c.getEmissor())));

        return response;
    }
}
