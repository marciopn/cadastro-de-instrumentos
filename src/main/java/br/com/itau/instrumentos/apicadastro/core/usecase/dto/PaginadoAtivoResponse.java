package br.com.itau.instrumentos.apicadastro.core.usecase.dto;

import br.com.itau.instrumentos.base.dto.response.BaseResponse;

import java.util.ArrayList;
import java.util.List;

public class PaginadoAtivoResponse extends BaseResponse {

    private Integer paginaAtual;
    private Integer qtdRegistrosTotais;
    private Integer qtdRegistrosDaPagina;
    private List<PaginadoAtivoResponseData> ativos = new ArrayList<>();

    public Integer getPaginaAtual() {
        return paginaAtual;
    }

    public void setPaginaAtual(Integer paginaAtual) {
        this.paginaAtual = paginaAtual;
    }

    public Integer getQtdRegistrosTotais() {
        return qtdRegistrosTotais;
    }

    public void setQtdRegistrosTotais(Integer qtdRegistrosTotais) {
        this.qtdRegistrosTotais = qtdRegistrosTotais;
    }

    public Integer getQtdRegistrosDaPagina() {
        return qtdRegistrosDaPagina;
    }

    public void setQtdRegistrosDaPagina(Integer qtdRegistrosDaPagina) {
        this.qtdRegistrosDaPagina = qtdRegistrosDaPagina;
    }

    public List<PaginadoAtivoResponseData> getAtivos() {
        return ativos;
    }

    public void adicionarAtivo(PaginadoAtivoResponseData ativoPaginado) {
        this.ativos.add(ativoPaginado);
    }
}
