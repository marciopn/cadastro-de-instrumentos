package br.com.itau.instrumentos.apicadastro.infra.entrypoint.http.controller;

import br.com.itau.instrumentos.apicadastro.core.usecase.*;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/ativos")
public class AtivoController extends BaseController {

    private final CadastrarAtivoUseCase cadastrarUseCase;
    private final DeletarAtivoUseCase deletarAtivoUseCase;
    private final BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase;
    private final AtualizarAtivoUseCase atualizarUseCase;
    private final BuscarAtivosUseCase buscarAtivosUseCase;

    public AtivoController(CadastrarAtivoUseCase cadastrarUseCase, DeletarAtivoUseCase deletarAtivoUseCase,
                           BuscarPorCodigoAtivoUseCase buscarPorCodigoAtivoUseCase, AtualizarAtivoUseCase atualizarUseCase,
                           BuscarAtivosUseCase buscarAtivosUseCase) {
        this.cadastrarUseCase = cadastrarUseCase;

        this.deletarAtivoUseCase = deletarAtivoUseCase;

        this.buscarPorCodigoAtivoUseCase = buscarPorCodigoAtivoUseCase;

        this.atualizarUseCase = atualizarUseCase;

        this.buscarAtivosUseCase = buscarAtivosUseCase;
    }

    @PostMapping(consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AtivoResponse> cadastrarAtivo(@RequestBody AtivoRequest request) {
        log.info("Cadastrando ativo " + request.getCodigo(), request);

        AtivoResponse response = cadastrarUseCase.executar(request);

        log.info("Resposta do cadastro para o ativo de código " + response.getCodigo() + ". ID gerado =  "
                + response.getId(), response);

        return construirResponse(response);
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<AtivoResponse> buscarPorCodigo(@PathVariable("codigo") String codigo) {
        log.info("Buscando ativo pelo código " + codigo);

        AtivoResponse response = buscarPorCodigoAtivoUseCase.executar(codigo);

        log.info("Resposta da busca para o ativo de código " + response.getCodigo() + ". ID =  " + response.getId(),
                response);

        return construirResponse(response);
    }

    @DeleteMapping(value = "/{codiigo}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AtivoResponse> deletarAtivo(@PathVariable("codigo") String codigo) {
        log.info("Deletando ativo " + codigo);

        AtivoResponse response = deletarAtivoUseCase.executar(codigo);

        return construirResponse(response);
    }

    @PutMapping(value = "/{codigo}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AtivoResponse> atualizarAtivo(@RequestBody AtivoRequest request,
                                                        @PathVariable("codigo") String codigo) {
        request.setCodigo(codigo);

        AtivoResponse response = atualizarUseCase.executar(request);
        return construirResponse(response);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaginadoAtivoResponse> buscarAtivos(@RequestParam("pagina") Integer pagina) {
        PaginadoAtivoResponse paginadoResponse = buscarAtivosUseCase.executar(pagina);

        return construirResponse(paginadoResponse);
    }

    @GetMapping("/public")
    public ResponseEntity<String> publicMethod(){
        return ResponseEntity.ok("Está tudo OK.");
    }
}