package br.com.itau.instrumentos.apicadastro.infra.dataprovider.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ativo")
public class JpaAtivoEntity extends JpaBaseEntity {

    @Column(name = "codigo", length = 10, nullable = false)
    private String codigo;

    @Column(name = "descricao", length = 255, nullable = false)
    private String descricao;

    @Column(name = "emissor", length = 255, nullable = false)
    private String emissor;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmissor() {
        return descricao;
    }

    public void setEmissor(String emissor) {
        this.emissor = descricao;
    }
}
