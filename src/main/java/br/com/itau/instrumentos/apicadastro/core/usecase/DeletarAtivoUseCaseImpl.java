package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.dto.response.ResponseDataErro;
import br.com.itau.instrumentos.base.gateway.DeletarGateway;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DeletarAtivoUseCaseImpl implements DeletarAtivoUseCase {

    private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

    private final DeletarGateway<String> deletarGateway;

    public DeletarAtivoUseCaseImpl(BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway,
                                   DeletarGateway<String> deletarGateway) {
        this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;

        this.deletarGateway = deletarGateway;
    }

    @Override
    public AtivoResponse executar(String codigo) {
        AtivoResponse response = new AtivoResponse();
        response.setCodigo(codigo);

        Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(codigo);

        if (opAtivo.isPresent()) {
            boolean deletado = deletarGateway.deletar(codigo);

            if (deletado) {
                response.setId(opAtivo.get().getId());

                response.setDescricao(opAtivo.get().getDescricao());

                response.setEmissor(opAtivo.get().getEmissor());
            }else {
                response.getResponse()
                        .adicionarErro(new ResponseDataErro(
                                "Ativo de código " + codigo + " não foi deletado. Favor contatar a equipe de suporte.",
                                ListaErroEnum.NAO_FOI_POSSIVEL_DELETAR));
            }
        } else {
            response.getResponse().adicionarErro(new ResponseDataErro("Ativo de código " + codigo + " não existe.",
                    ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
        }

        return response;
    }
}