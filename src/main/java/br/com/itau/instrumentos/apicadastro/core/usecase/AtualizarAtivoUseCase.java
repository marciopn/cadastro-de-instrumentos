package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.base.usecase.BaseUseCase;

public interface AtualizarAtivoUseCase extends BaseUseCase<AtivoRequest, AtivoResponse> {
}
