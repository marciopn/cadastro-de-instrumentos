package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.dto.response.ResponseDataErro;
import br.com.itau.instrumentos.base.gateway.SalvarGateway;
import org.springframework.stereotype.Service;

@Service
public class CadastrarAtivoUseCaseImpl extends BaseAtivoUseCase implements CadastrarAtivoUseCase {

    private final SalvarGateway<Ativo> salvarGateway;

    private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

    public CadastrarAtivoUseCaseImpl(SalvarGateway<Ativo> salvarGateway,
                                     BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {
        this.salvarGateway = salvarGateway;

        this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
    }

    @Override
    public AtivoResponse executar(AtivoRequest input) {
        AtivoResponse response = new AtivoResponse();

        response.setCodigo(input.getCodigo());

        response.setDescricao(input.getDescricao());

        response.setEmissor(input.getEmissor());

        validarCamposObrigatorios(input, response);

        if (response.getResponse().getErros().size() > 0) {
            return response;
        }

        if (buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo()).isPresent()) {
            response.getResponse().adicionarErro(new ResponseDataErro("Ativo já cadastrado com código " + input.getCodigo(), ListaErroEnum.DUPLICIDADE));
        } else {
            Ativo ativo = new Ativo();

            ativo.setCodigo(input.getCodigo());

            ativo.setDescricao(input.getDescricao());

            ativo.setEmissor(input.getEmissor());

            salvarGateway.salvar(ativo);

            response.setId(ativo.getId());
        }

        return response;
    }
}
