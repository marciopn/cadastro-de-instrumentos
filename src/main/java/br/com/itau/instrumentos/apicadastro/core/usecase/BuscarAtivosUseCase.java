package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.usecase.dto.PaginadoAtivoResponse;
import br.com.itau.instrumentos.base.usecase.BaseUseCase;

public interface BuscarAtivosUseCase extends BaseUseCase<Integer, PaginadoAtivoResponse> {
}