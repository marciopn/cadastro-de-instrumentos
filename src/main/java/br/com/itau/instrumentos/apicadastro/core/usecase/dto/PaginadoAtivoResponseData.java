package br.com.itau.instrumentos.apicadastro.core.usecase.dto;

public class PaginadoAtivoResponseData {

    private String id;

    private String codigo;

    private String descricao;

    private String emissor;

    public PaginadoAtivoResponseData() {

    }

    public PaginadoAtivoResponseData(String id, String codigo, String descricao, String emissor) {
        super();

        this.id = id;

        this.codigo = codigo;

        this.descricao = descricao;

        this.emissor = emissor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmissor() {
        return emissor;
    }

    public void setEmissor(String emissor) {
        this.emissor = emissor;
    }
}
