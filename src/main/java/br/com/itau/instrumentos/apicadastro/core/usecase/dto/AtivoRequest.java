package br.com.itau.instrumentos.apicadastro.core.usecase.dto;

public class AtivoRequest {

    private String codigo;

    private String descricao;

    private String emissor;

    public AtivoRequest(){

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmissor() {
        return emissor;
    }

    public void setEmissor(String emissor) {
        this.emissor = emissor;
    }

}
