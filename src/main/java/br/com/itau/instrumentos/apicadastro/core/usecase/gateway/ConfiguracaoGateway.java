package br.com.itau.instrumentos.apicadastro.core.usecase.gateway;

public interface ConfiguracaoGateway {
    Integer getQuantidadeRegistrosPorPagina();
}
