package br.com.itau.instrumentos.apicadastro.core.usecase.dto;

import br.com.itau.instrumentos.base.dto.response.BaseResponse;

public class AtivoResponse extends BaseResponse {

    private String id;

    private String codigo;

    private String descricao;

    private String emissor;

    public AtivoResponse() {

    }

    public AtivoResponse(String id, String codigo, String descricao, String emissor) {
        this.id = id;
        this.codigo = codigo;
        this.descricao = descricao;
        this.emissor = emissor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmissor() {
        return emissor;
    }

    public void setEmissor(String emissor) {
        this.emissor = emissor;
    }
}