package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.apicadastro.core.entity.Ativo;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;
import br.com.itau.instrumentos.apicadastro.core.usecase.gateway.BuscarPorCodigoAtivoGateway;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.dto.response.ResponseDataErro;
import br.com.itau.instrumentos.base.gateway.SalvarGateway;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AtualizarAtivoUseCaseImpl extends BaseAtivoUseCase implements AtualizarAtivoUseCase {

    private final SalvarGateway<Ativo> salvarGateway;

    private final BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway;

    public AtualizarAtivoUseCaseImpl(SalvarGateway<Ativo> salvarGateway,
                                     BuscarPorCodigoAtivoGateway buscarPorCodigoAtivoGateway) {

        this.salvarGateway = salvarGateway;

        this.buscarPorCodigoAtivoGateway = buscarPorCodigoAtivoGateway;
    }

    @Override
    public AtivoResponse executar(AtivoRequest input) {
        logger.info("Iniciando atualização do ativo de código " + input.getCodigo() + " para descrição "
                + input.getDescricao() + " e emissor " + input.getEmissor());

        AtivoResponse response = new AtivoResponse();

        response.setCodigo(input.getCodigo());

        response.setDescricao(input.getDescricao());

        response.setEmissor(input.getEmissor());

        validarCamposObrigatorios(input, response);

        if (response.getResponse().getErros().size() > 0) {
            return response;
        }

        Optional<Ativo> opAtivo = buscarPorCodigoAtivoGateway.buscarPorCodigoAtivo(input.getCodigo());

        if (opAtivo.isPresent()) {
            Ativo ativo = opAtivo.get();

            ativo.setDescricao(input.getDescricao());

            salvarGateway.salvar(ativo);

            response.setId(ativo.getId());
        } else {
            String msg = "Ativo não encontrado com código " + input.getCodigo();

            logger.error(msg);

            response.getResponse().adicionarErro(new ResponseDataErro(msg, ListaErroEnum.ENTIDADE_NAO_ENCONTRADA));
        }

        return response;
    }
}

