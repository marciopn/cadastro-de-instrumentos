package br.com.itau.instrumentos.apicadastro.infra.entrypoint.http.controller;

import br.com.itau.instrumentos.base.dto.response.BaseResponse;
import br.com.itau.instrumentos.base.dto.response.ListaErroEnum;
import br.com.itau.instrumentos.base.dto.response.ResponseDataErro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public abstract class BaseController {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    protected <T extends BaseResponse> ResponseEntity<T> construirResponse(T response) {

        log.debug("Iniciando a construção do response HTTP");

        if (response.getResponse().getErros().isEmpty()) {
            log.info("Resposta sem ERROS (200 - OK)", response);

            return ResponseEntity.ok(response);
        } else {
            log.error("Ocorreram erros ao processar sua requisição", response);

            List<ResponseDataErro> erros = response.getResponse().getErros();

            ResponseDataErro erro = erros.get(0);

            if (erro.getTipo().equals(ListaErroEnum.CAMPOS_OBRIGATORIOS)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            } else if (erro.getTipo().equals(ListaErroEnum.DUPLICIDADE)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
            } else if (erro.getTipo().equals(ListaErroEnum.ENTIDADE_NAO_ENCONTRADA)) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
            }
        }
    }
}