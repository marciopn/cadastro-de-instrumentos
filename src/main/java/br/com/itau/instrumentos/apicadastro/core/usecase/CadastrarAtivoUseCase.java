package br.com.itau.instrumentos.apicadastro.core.usecase;

import br.com.itau.instrumentos.base.usecase.BaseUseCase;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoRequest;
import br.com.itau.instrumentos.apicadastro.core.usecase.dto.AtivoResponse;

public interface CadastrarAtivoUseCase extends BaseUseCase<AtivoRequest, AtivoResponse> {
}
