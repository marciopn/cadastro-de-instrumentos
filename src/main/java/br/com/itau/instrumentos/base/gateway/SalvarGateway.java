package br.com.itau.instrumentos.base.gateway;

public interface SalvarGateway<T> {

    T salvar(T t);

}
