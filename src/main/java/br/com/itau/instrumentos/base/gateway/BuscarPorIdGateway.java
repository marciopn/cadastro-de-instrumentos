package br.com.itau.instrumentos.base.gateway;

import java.util.Optional;

public interface BuscarPorIdGateway<T,ID> {

    Optional<T> buscarPorId(ID id);

}
