package br.com.itau.instrumentos.base.usecase;

public interface BaseUseCase<I,O> {
    O executar(I input);
}
